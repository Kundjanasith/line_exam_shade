import json
import sys
with open('../data/exam_data1.json', 'r') as f:
    ex_dict = json.load(f)
sentences = []
for i in ex_dict:
    sentences.append(i[1])
from sklearn.feature_extraction.text import CountVectorizer
vectorizer = CountVectorizer(min_df=0, lowercase=False)
vectorizer.fit(sentences)
vectorizer.vocabulary_
print(vectorizer.vocabulary_)
vectorizer.transform(sentences).toarray()
from sklearn.model_selection import train_test_split
y = []
for i in ex_dict:
    y.append(i[0])
sentences_train, sentences_test, y_train, y_test = train_test_split(sentences, y, test_size=0.25, random_state=1000)
# from sklearn.feature_extraction.text import CountVectorizer
# vectorizer = CountVectorizer()
# vectorizer.fit(sentences_train)
# X_train = vectorizer.transform(sentences_train)
# X_test  = vectorizer.transform(sentences_test)
# print(type(X_train))
# print(type(X_test))
# print(type(y_train))
# print(type(y_test))
# import scipy.sparse
# import numpy as np
# scipy.sparse.save_npz('/tmp/sparse_matrix.npz', sparse_matrix)
# sparse_matrix = scipy.sparse.load_npz('/tmp/sparse_matrix.npz')
# scipy.sparse.save_npz('X_train.npz', X_train)
# scipy.sparse.save_npz('X_test.npz', X_test)
# import pickle
# with open('outfile', 'wb') as fp:
#     pickle.dump(itemlist, fp)
# with open ('outfile', 'rb') as fp:
#     itemlist = pickle.load(fp)
# with open('y_train.npz', 'wb') as fp:
#     pickle.dump(y_train, fp)
# with open('y_test.npz', 'wb') as fp:
#     pickle.dump(y_test, fp)

from keras.models import Sequential
from keras import layers
input_dim = X_train.shape[1]
model = Sequential()
model.add(layers.Dense(10, input_dim=input_dim, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary()
history = model.fit(X_train, y_train,epochs=10,verbose=False,validation_data=(X_test, y_test),batch_size=10)
model.save_weights('rnn.h5')
loss, accuracy = model.evaluate(X_train, y_train, verbose=False)
print("Training Accuracy: {:.4f}".format(accuracy))
loss, accuracy = model.evaluate(X_test, y_test, verbose=False)
print("Testing Accuracy:  {:.4f}".format(accuracy))