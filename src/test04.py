import scipy.sparse
import numpy as np
import pickle
import sys
import json
import sys
with open('../data/exam_data1.json', 'r') as f:
    ex_dict = json.load(f)
sentences = []
for i in ex_dict[:100]:
    sentences.append(i[1])
from sklearn.feature_extraction.text import CountVectorizer
vectorizer = CountVectorizer(min_df=0, lowercase=False)
vectorizer.fit(sentences)
vectorizer.vocabulary_
print(vectorizer.vocabulary_)
vectorizer.transform(sentences).toarray()
from sklearn.model_selection import train_test_split
y = []
for i in ex_dict[:100]:
    y.append(i[0])
sentences_train, sentences_test, y_train, y_test = train_test_split(sentences, y, test_size=0.2)
from sklearn.feature_extraction.text import CountVectorizer
vectorizer = CountVectorizer()
vectorizer.fit(sentences_train)
X_train = vectorizer.transform(sentences_train)
X_test  = vectorizer.transform(sentences_test)
filename = 'finalized_model.sav'
loaded_model = pickle.load(open(filename, 'rb'))
result = loaded_model.score(X_test, y_test)
print(result)