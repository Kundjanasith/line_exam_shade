import scipy.sparse
import numpy as np
import pickle
import sys
X_train = scipy.sparse.load_npz('X_train.npz')
X_test = scipy.sparse.load_npz('X_test.npz')
with open ('y_train.npz', 'rb') as fp:
    y_train = pickle.load(fp)
with open ('y_test.npz', 'rb') as fp:
    y_test = pickle.load(fp)
print(type(X_train))
print(type(X_test))
print(type(y_train))
print(type(y_test))
from keras.models import Sequential
from keras import layers
input_dim = X_train.shape[1]
print(input_dim)
model = Sequential()
model.add(layers.Dense(10, input_dim=input_dim, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary()
print(X_train.shape)
print(X_test.shape)
print(np.array(y_train).shape)
print(np.array(y_test).shape)
# for i in X_train.toarray():
#     print(i.shape)
#     sys.exit()
X_train = X_train.toarray()
X_test = X_test.toarray()
y_train = np.array(y_train)
y_test = np.array(y_test)
history = model.fit(X_train, y_train,epochs=10,verbose=False,validation_data=(X_test, y_test),batch_size=10)