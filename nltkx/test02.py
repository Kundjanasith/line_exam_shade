import json
import numpy as np
with open('../data/exam_data1.json', 'r') as f:
    ex_dict = json.load(f)
doc_list_name = []
title_list_name = []
doc_list_content = []
title_list_content = []
for i in ex_dict:
    if i[0][:3] == 'doc':
        doc_list_name.append(i[0])
        doc_list_content.append(i[1])
    else:
        title_list_name.append(i[0])
        title_list_content.append(i[1])
doc_list_name = np.array(doc_list_name)
title_list_name = np.array(title_list_name)
doc_list_content = np.array(doc_list_content)
title_list_content = np.array(title_list_content)

with open('../data/train_q.json', 'r') as f:
    train_dict = json.load(f)
train_title = []
train_content = []
r = len(train_dict)
count = 0
for i in train_dict:
    res = np.where(title_list_name==i['title_id'])
    res = title_list_content[res]
    res1 = np.where(doc_list_name==i['ans_id'])
    res1 = doc_list_content[res1]
    count = count + 1
    print(str(count)+"/"+str(r))
    train_title.append(res[0].encode("utf-8"))
    train_content.append(res1[0].encode("utf-8"))
train_title_file = open("train_title.csv","w")
for i in train_title:
    train_title_file.write(i+"\n")
train_content_file = open("train_content.csv","w")
for i in train_content:
    train_content_file.write(i+"\n")