import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
vectorizer = CountVectorizer()
train_title = open('train_title.csv', 'r') 
train_title_list = []
for i in train_title.readlines():
    train_title_list.append(i)
train_content = open('train_content.csv', 'r') 
train_content_list = []
for i in train_content.readlines():
    train_content_list.append(i)
vectorizerT = CountVectorizer()
vectorizerT.fit(train_title_list)
x_train = vectorizerT.transform(train_title_list)
vectorizerC = CountVectorizer()
vectorizerC.fit(train_content_list)
y_train = vectorizerT.transform(train_content_list)
print(x_train.shape)
print(y_train.shape)
from keras.models import Sequential
from keras import layers
model = Sequential()
input_dim = x_train.shape[1]
output_dim = y_train.shape[1]
model.add(layers.Dense(100, input_dim=input_dim, activation='relu'))
model.add(layers.Dense(10))
model.add(layers.Dense(output_dim, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary()
model.fit([x_train], [y_train],epochs=30,verbose=True,batch_size=10)
model.save_weights('model_01.h5')