import json
import sys
import numpy as np
from keras.optimizers import SGD
import pandas as pd
with open('../data/exam_data1.json', 'r') as f:
    ex_dict = json.load(f)
doc_list_name = []
title_list_name = []
doc_list_content = []
title_list_content = []
for i in ex_dict:
    if i[0][:3] == 'doc':
        doc_list_name.append(i[0])
        doc_list_content.append(i[1])
    else:
        title_list_name.append(i[0])
        title_list_content.append(i[1])
from sklearn.feature_extraction.text import CountVectorizer
vectorizer_doc = CountVectorizer(min_df=0, lowercase=False)
vectorizer_doc.fit(doc_list_content)
array_doc = vectorizer_doc.transform(doc_list_content).toarray()
print(array_doc.shape)
vectorizer_tit = CountVectorizer(min_df=0, lowercase=False)
vectorizer_tit.fit(title_list_content)
array_tit = vectorizer_doc.transform(title_list_content).toarray()
print(array_tit.shape)