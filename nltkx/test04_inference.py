from keras.models import Sequential
from keras import layers
model = Sequential()
input_dim = x_train.shape[1]
output_dim = y_train.shape[1]
model.add(layers.Dense(100, input_dim=input_dim, activation='relu'))
model.add(layers.Dense(10))
model.add(layers.Dense(output_dim, activation='sigmoid'))
model.load_weights()