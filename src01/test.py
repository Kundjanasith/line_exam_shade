import json
import sys
import numpy as np
from keras.optimizers import SGD
import pandas as pd
with open('../data/exam_data1.json', 'r') as f:
    ex_dict = json.load(f)
doc_list_name = []
title_list_name = []
doc_list_content = []
title_list_content = []
for i in ex_dict:
    if i[0][:3] == 'doc':
        doc_list_name.append(i[0])
        doc_list_content.append(i[1])
    else:
        title_list_name.append(i[0])
        title_list_content.append(i[1])
from sklearn.feature_extraction.text import CountVectorizer
vectorizer_doc = CountVectorizer(min_df=0, lowercase=False)
vectorizer_doc.fit(doc_list_content)
array_doc = vectorizer_doc.transform(doc_list_content).toarray()
print(array_doc.shape)
s = np.unique(array_doc)
print(s.shape)
vectorizer_tit = CountVectorizer(min_df=0, lowercase=False)
vectorizer_tit.fit(title_list_content)
array_tit = vectorizer_doc.transform(title_list_content).toarray()
print(array_tit.shape)


# df = pd.read_csv('trainX.txt',header=None)
# train_tem_arr = []
# test_tem_arr = []
# r = type(df.iterrows())
# print(r)
# count = 0
# for index, row in df.iterrows():
#     # print(row[0], row[1])
#     train_tem_arr.append(array_tit[row[0]])
#     test_tem_arr.append(array_doc[row[1]])
#     count = count + 1
#     print(str(count)+"/"+str(r))
# train_tem_arr = np.array(train_tem_arr)
# test_tem_arr = np.array(test_tem_arr)
# print(train_tem_arr.shape)
# print(test_tem_arr.shape)
    